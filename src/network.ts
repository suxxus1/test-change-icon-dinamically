export const getDeliveries = (user: number) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      const data = {
        name: user === 1 ? "John" : "Alice",
        theme: `th-${user}`,
      };

      resolve(data);
    }, 2000);
  });
};
