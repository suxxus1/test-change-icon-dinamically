import { createPortal } from "react-dom";
import { useState } from "react";

const wp = {
  title: "wealth platform",
  apple: "wp/icon_azul.png",
  iconPng: "wp/icon_rosa.png",
  maskIcon: "wp/icon.svg",
  iconSvg: "wp/icon.svg",
};

const sh = {
  title: "schroders platform",
  apple: "sh/icon_rosa.png",
  iconPng: "sh/icon_verde.png",
  maskIcon: "sh/icon.svg",
  iconSvg: "sh/icon.svg",
};

function App() {
  const [theme, setTheme] = useState(wp);

  return (
    <>
      <header>
        <link rel="apple-touch-icon" href={theme.apple} />
        <link rel="icon" type="image/png" href={theme.iconPng} />
        <link rel="mask-icon" href={theme.maskIcon} color="#adff2f" />
        <link rel="icon" type="image/svg+xml" href={theme.iconSvg} />
        <title>{theme.title}</title>
      </header>
      <button
        onClick={() => {
          setTheme(wp);
        }}
      >
        wp theme
      </button>
      <button
        onClick={() => {
          setTheme(sh);
        }}
      >
        sh theme
      </button>
    </>
  );
}
export default App;
